﻿﻿using System.Collections;
using System.Collections.Generic;
 using System.Numerics;
 using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
 using Plane = UnityEngine.Plane;
 using Quaternion = UnityEngine.Quaternion;
 using Vector2 = UnityEngine.Vector2;
 using Vector3 = UnityEngine.Vector3;

public class HandTool : MonoBehaviour
{   
    public GameObject LoadingSlider;
    [FormerlySerializedAs("cursorTexture")] public Texture2D openCursorHand;
    public Texture2D closeCursorHand;
    [SerializeField]
    public bool ResetRotation = true;
    public bool StayAbove = true;
    
    private float _colliderHeight;
    private float _maxHeightDetected;
    private GameObject destroySliderInstance = null;
    private Ray ray;
    private Plane plane;
    private Vector3 offset;
    private Vector3 _planePos;
    private GameObject _draggingObject;
    private CameraDirector camDir;
    private static IEnumerator coroutine;
    private ToolManager tManager;
    private bool hasStartedToDestroy = false;
    public static HandTool instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);    

    }

    
    void Start()
    {
        camDir = FindObjectOfType<CameraDirector>();
        //_draggingStartPos = transform.position;
        tManager = FindObjectOfType<ToolManager>();
        plane = new Plane(Vector3.up, _planePos);

    }

    private void Update()
    {

        if (camDir.isHandActive)
        {
            Hand();
        }

    }

    private void Hand()
    {
        bool isCtrlKeyDown =Input.GetKey(KeyCode.LeftControl);
        if (isCtrlKeyDown)
        {
            if (hasStartedToDestroy == false)
            {
                Cursor.SetCursor(openCursorHand, Vector2.zero, CursorMode.Auto);

            }

            tManager.Stop();

            MouseDragging();

        }
        else
        {

            tManager.Resume();
        }

    }
    
    private void MouseDragging()
    {

        _planePos = GameObject.Find("Floor").transform.position;
        _planePos.y -= .5f;

        if (Input.GetMouseButtonDown(0))
        {
            
            if (hasStartedToDestroy)
            {
                Cursor.visible = false;
            }
            else
            {
                SetDraggingObject();

            }

        }
        else if (Input.GetMouseButton(0))
        {

            if (hasStartedToDestroy)
            {
                Cursor.visible = false;
            }
            else
            {
                DragClickedObject();

            }

        }
        else if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("on mouse up");
            if (hasStartedToDestroy)
            {
                StopDestroyingLair();
            }
            else
            {
                Cursor.SetCursor(openCursorHand, Vector2.zero, CursorMode.Auto);
                ResetTarget();

            }


        }


    }

    void SetDraggingObject()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        int layer_mask = LayerMask.GetMask("ObjectRaycast");
        float distance; // the distance from the ray origin to the ray intersection of the plane



        if (Physics.Raycast(ray, out hit, Mathf.Infinity,layer_mask) || hit.collider != null)
        {
            if (hit.collider.gameObject.GetComponent<Forniture>() != null)
            {
                // clicking on forniture
                Cursor.SetCursor(closeCursorHand, Vector2.zero, CursorMode.Auto);
                _draggingObject = hit.collider.gameObject;
                hit.collider.tag = "Selected";
                SetDraggingOffset(out distance);
            }
            else if(hit.collider.transform.parent.GetComponent<Forniture>() != null)
            {
                // clicking composite object
                Cursor.SetCursor(closeCursorHand, Vector2.zero, CursorMode.Auto);
                _draggingObject = hit.collider.transform.parent.gameObject;
                _draggingObject.tag = "Selected";
                SetDraggingOffset(out distance);
            }else if (hit.collider.gameObject.GetComponent<MouseSpawn>() != null)
            {
                // clicked lair start to destroying
                var screenPos = Camera.main.WorldToScreenPoint(hit.collider.transform.position);
                hasStartedToDestroy = true;

                Debug.Log("Clicked on nest!");
                DestroyLair(screenPos, hit.collider.gameObject);
            }
        }
    }
    
    void DragClickedObject()
    {

        //draggingHeight = _draggingObject.GetComponent<FornitureScript>().GetExtents();
        if (_draggingObject != null)
        {

            Cursor.SetCursor(closeCursorHand, Vector2.zero, CursorMode.Auto);

            if (StayAbove)
            {
                DragAboveObstacles();
            }
            else
            {
                DragAgainstObstacles();
            }
                
            if (ResetRotation)_draggingObject.transform.rotation = Quaternion.identity;

        }
    }

    private void SetDraggingOffset(out float distance)
    {
        // sets offset in order to avoid object to center to mouse position on click
        if (plane.Raycast(ray, out distance) && _draggingObject.CompareTag("Selected"))
        {
            offset = ray.GetPoint(distance) - _draggingObject.transform.position;
        }
    }
    
    private void DragAgainstObstacles()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float distance; // the distance from the ray origin to the ray intersection of the plane


        if (plane.Raycast(ray, out distance)&&_draggingObject.CompareTag("Selected"))
        {
            var mouseWorld = ray.GetPoint(distance) - offset;
            _draggingObject.transform.position = new Vector3(mouseWorld.x, _draggingObject.transform.position.y, mouseWorld.z);
            //_draggingObject.transform.position = _draggingObject.transform.position;

        }
    }

    private void DragAboveObstacles()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane plane = new Plane(Vector3.up, _planePos);

        float distance; // the distance from the ray origin to the ray intersection of the plane


        if (plane.Raycast(ray, out distance)&& _draggingObject.CompareTag("Selected"))
        {
            var mouseWorldX = ray.GetPoint(distance).x;
            var mouseWorldZ = ray.GetPoint(distance).z;

            PositionAboveObject();
            float verticalSpace = .3f;
            _draggingObject.transform.position = new Vector3(mouseWorldX, _maxHeightDetected + verticalSpace, mouseWorldZ);
        }


    }
    
    private void PositionAboveObject()
    {
        // Finds higher position between near object in order to go above them
        Vector3 highPos;

        // Create overlapbox of the size of the dragged object
        float _draggingHeight = _draggingObject.GetComponent<Collider>().bounds.size.y;
        float _draggingWidht = _draggingObject.GetComponent<Collider>().bounds.size.x;
        float _draggingDeep = _draggingObject.GetComponent<Collider>().bounds.size.z;
        Vector3 _draggingCenter = _draggingObject.GetComponent<MeshRenderer>().bounds.center;
        Vector3 overlapBoxSize = new Vector3(_draggingWidht + .5f, _draggingHeight + 1f, _draggingDeep + .5f);
        Collider[] hitColliders = Physics.OverlapBox(_draggingCenter, overlapBoxSize, Quaternion.identity);
        
        _maxHeightDetected = 0;

        int i = 0;
        while (i < hitColliders.Length)
        {
            if (!hitColliders[i].CompareTag("Selected") && !hitColliders[i].CompareTag("Mouse"))
            {
                highPos = hitColliders[i].transform.position;

                Collider rend = hitColliders[i].gameObject.GetComponent<Collider>();
                
                //STREGONERIA
                //new heigth is 
                // collider pos + half it's height = center of dragging to border
                //we must go up by half dragging object height
                _colliderHeight = highPos.y + _draggingObject.GetComponent<Collider>().bounds.size.y/2 + rend.bounds.size.y/2;


                //Debug.Log("Detected  " + hitColliders[i].name);
                if (_colliderHeight > _maxHeightDetected)
                {
                    _maxHeightDetected = _colliderHeight;
                }
            }

            i++;
        }

    }

    private void DestroyLair(Vector3 screenPos, GameObject nest)
    {
        LoadingSlider.SetActive(true);
        LoadingSlider.GetComponent<SliderBehavior>().BindToNest(nest);
        coroutine = LoadingBar(10f, nest);

        StartCoroutine(coroutine);

    }


    public void StopDestroyingLair()
    {
        Cursor.visible = false;
        StopCoroutine(coroutine);
        LoadingSlider.GetComponent<Slider>().value = 10;

        LoadingSlider.SetActive(false);
        hasStartedToDestroy = false;

    }
    

    private IEnumerator LoadingBar(float waitTime, GameObject nest)
    {

        for (float i = waitTime; i>=0; i--)
        {
            LoadingSlider.GetComponent<Slider>().value = i;
            yield return new WaitForSeconds(1f);

        }
        
        Debug.Log("Destroying nest");
        LoadingSlider.GetComponent<Slider>().value = 10;

        LoadingSlider.SetActive(false);

        
        //Destroy(destroySliderInstance);
        Destroy(nest);
        yield return new WaitForSeconds(0.1f);
    }

    void ResetTarget()
    {

        offset = Vector3.zero;
        _maxHeightDetected = 0;
        _colliderHeight = 0;
        _draggingObject.tag = "Untagged";
        _draggingObject = null;
    }


}
