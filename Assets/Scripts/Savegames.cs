﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class Savegame
{
    public List<Save> saves = new List<Save>();

    public void AddSave(Save save)
    {
        saves.Add(save);
    }
}
