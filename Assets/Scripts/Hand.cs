﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    private Vector3 offset;

    private float _maxHeightDetected;
    private float _colliderHeight;

    // Update is called once per frame
    void Update()
    {
        PositionAboveObject();

        transform.position = new Vector3(transform.position.x, _maxHeightDetected, transform.position.z);
        //FaceClosestObject();

    }

    private void FaceClosestObject()
    {
        GameObject closest;
        Forniture[] allForniture = FindObjectsOfType<Forniture>();
        foreach (Forniture forni in allForniture)
        {
            float distance = Vector3.Distance(transform.position, forni.gameObject.transform.position);
            if (distance < 10f)
            {
               // transform.LookAt(forni.transform.position);
                DragBelowObject(forni.gameObject);
            }
        }
    }

    private GameObject _draggingObject;
    private void DragBelowObject(GameObject forni)
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && forni.name != "floor" && forni.name != "lair_mouse")
        {
            SetDraggingOffset(forni);
            _draggingObject = forni;

        }
//        else if (Input.GetKey(KeyCode.Mouse0) && forni.name == "lair_mouse")
//        {
//            Debug.Log("Sbam");
//            GetComponent<Animator>().SetBool("Sbam", true);
//        }
        else if (Input.GetKey(KeyCode.Mouse0) && forni.name != "lair_mouse" && forni.name != "floor")
        {
            GetComponent<Animator>().SetBool("Closed", true);
            _draggingObject.transform.position = new Vector3(transform.position.x - offset.x, forni.transform.position.y,
                transform.position.z - offset.z);
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            GetComponent<Animator>().SetBool("Closed", false);
            GetComponent<Animator>().SetBool("Sbam", false);

            _draggingObject = null;
        }
    }

    private void SetDraggingOffset(GameObject draggingObject)
    {
        // sets offset in order to avoid object to center to mouse position on click
        offset = transform.position - draggingObject.transform.position;
    }

    private void PositionAboveObject()
    {
        // Finds higher position between near object in order to go above them
        Vector3 highPos;

        // Create overlapbox of the size of the dragged object
        float _draggingHeight = GetComponent<Collider>().bounds.size.y;
        float _draggingWidht = GetComponent<Collider>().bounds.size.x;
        float _draggingDeep = GetComponent<Collider>().bounds.size.z;
        Vector3 _draggingCenter = GetComponent<Collider>().bounds.center;
        Vector3 overlapBoxSize = new Vector3(_draggingWidht, _draggingHeight, _draggingDeep);
        Collider[] hitColliders = Physics.OverlapBox(_draggingCenter, overlapBoxSize, Quaternion.identity);
        
        _maxHeightDetected = 0;
        transform.rotation = Quaternion.identity;

        int i = 0;
        while (i < hitColliders.Length)
        {
            if (!hitColliders[i].CompareTag("Hand") 
                && !hitColliders[i].CompareTag("Mouse") 
                && hitColliders[i].name != "wall" 
                && hitColliders[i].name != "marciapiede" 
                && hitColliders[i].name != "ignore" 

                && hitColliders[i].name != "collider")
            {
                //Debug.Log("Rilevo altezza " + hitColliders[i].name);
                highPos = hitColliders[i].transform.position;

                Collider rend = hitColliders[i].gameObject.GetComponent<Collider>();
                
                //STREGONERIA
                //new heigth is 
                // collider pos + half it's height = center of dragging to border
                //we must go up by half dragging object height
                _colliderHeight = highPos.y + GetComponent<Collider>().bounds.size.y/2 + rend.bounds.size.y/2;

                _colliderHeight -= 1f;
                GetComponent<ToolRotation>().enabled = true;

                //Debug.Log("Detected  " + hitColliders[i].name);
                if (_colliderHeight > _maxHeightDetected)
                {
                    if (hitColliders[i].gameObject.name != "floor")
                    {
                        GetComponent<ToolRotation>().enabled = false;
                        transform.LookAt(highPos);
                    }

                    _maxHeightDetected = _colliderHeight;
                    if (hitColliders[i].gameObject.name == "GameObject (4)")
                    {
                        DragBelowObject(hitColliders[i].gameObject.transform.parent.gameObject);

                    }
                    else
                    {
                        DragBelowObject(hitColliders[i].gameObject);

                    }

                }
            }

            i++;
        }

    }

}
