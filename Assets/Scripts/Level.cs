﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Level : MonoBehaviour 
{
    public float transitionDuration = 2.5f;

    public UnityEvent GameStarted;
    public UnityEvent GameEnded;
    private List<Rigidbody> allRigidbody;

    private Forniture[] allforniture;
    void OnMouseOver()
    {
        transform.localScale = Vector3.one*1.2f;

    }

    private void Awake()
    {
        allforniture = FindObjectsOfType<Forniture>();
        foreach (var forniture in allforniture)
        {
           forniture.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            foreach (var rig in forniture.GetComponentsInChildren<Rigidbody>())
            {
                rig.isKinematic = true;
            }

        }
       
        SetKinematics();
    }

    private void OnMouseExit()
    {
        transform.localScale = Vector3.one;
    }

    private void OnMouseDown()
    {

        StartCoroutine(Transition());
        GetComponent<BoxCollider>().enabled = false;
        //LevelManager.instance.StartLevel();
    
        GameStarted.Invoke();
    }

    public void SetKinematics()
    {
        foreach (Rigidbody rig in allRigidbody)
        {
            rig.isKinematic = true;
        }
    }
    public void UnsetKinematics()
    {
        allforniture = FindObjectsOfType<Forniture>();
        foreach (var forniture in allforniture)
        {
            forniture.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            foreach (var rig in forniture.GetComponentsInChildren<Rigidbody>())
            {
                rig.isKinematic = false;
            }

        }
    }

    IEnumerator Transition()
    {
        
        //Transition between actual position and stage position
        float t = 0.0f;
        Vector3 startingPos = CameraDirector.instance.transform.position;
        while (t < 1.0f)
        {
            t += Time.deltaTime * (Time.timeScale/transitionDuration);


            CameraDirector.instance.transform.position = Vector3.Lerp(startingPos, new Vector3(transform.position.x, transform.position.y + 120, transform.position.z - 140), t);
            yield return 0;
        }

    }


}