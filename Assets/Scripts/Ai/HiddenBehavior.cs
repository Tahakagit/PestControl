﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenBehavior : StateMachineBehaviour
{
    private bool lairPresent;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<MouseScript>().Stop();
        Collider[] hitColliders = Physics.OverlapSphere(animator.gameObject.transform.position, 10f);
        foreach (var coll in hitColliders)
        {
            if (coll.gameObject.GetComponent<MouseSpawn>() != null)
            {
                lairPresent = true;
            }
        }

        if (!lairPresent)
        {
            animator.gameObject.GetComponent<MouseScript>().StartLairCreation();
 
        }
    }

//    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
//    {
//        
//    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<MouseScript>().StopLairCreation();

    }


    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
