﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class MouseScript : MonoBehaviour
{
    private NavMeshAgent _agent;
    private IEnumerator coroutine;
    public GameObject spawner;
    public float wanderRadius;
    public float wanderTimer;
    private float timer;

    public GameObject exposedIcn;
    public GameObject unexposedIcn;
    public float agentSpeed;
    public GameObject target;
    private GameObject hammer;
    public float DistanceRun;
    public bool exposed;
    public NavMeshData m_NavMeshData;
    private NavMeshDataInstance m_NavMeshInstance;

    private Animator _State;
    private float value = 50;

    
    void Start()
    {
        target = GameObject.Find("Target");
        _agent = GetComponent<NavMeshAgent>();
        _agent.speed = agentSpeed;
        _State = GetComponent<Animator>();
    }

    void OnEnable () {
        m_NavMeshInstance = NavMesh.AddNavMeshData(m_NavMeshData);
        timer = wanderTimer;

    }

    private void OnCollisionEnter(Collision other)
    {
        //Debug.Log("Collided " + other.collider.name);
        if (other.collider.name == "Cube")
        {
            Destroy(gameObject);

        }
    }

    
    void Update()
    {
        
//        Debug.Log(_levelManager.Started);
        if (GameDirector.instance.GameStarted)
        {
            _State.SetBool("Disabled", false);
            Act();
        }
        else
        {
            _State.SetBool("Disabled", true);

        }
    }
     void Act()
     {
         hammer = GameObject.Find("MountPoint");
 
         float distance = Vector3.Distance(transform.position, hammer.transform.position);
         if (distance<DistanceRun)
         {
             _State.SetBool("AmIChased", true);
         }
         else
         {
             _State.SetBool("AmIChased", false);
 
         }
 
         AmIVisible();
     
 /*
         if (exposed)
         {
 
             exposedIcn.SetActive(true);
             exposedIcn.transform.LookAt(Camera.main.transform.position);
             unexposedIcn.SetActive(false);
         }
         else
         {
             exposedIcn.SetActive(false);
             unexposedIcn.SetActive(true);
             unexposedIcn.transform.LookAt(Camera.main.transform.position);
 
 
         }
 */
 
     }

     private void AmIVisible()
     {
         RaycastHit hitInfo;
         Vector3 dir = Camera.main.transform.position - transform.position;
 
         //dir += Vector3.up * 10;
         if (Physics.Raycast(transform.position, dir, out hitInfo))
         {
             Debug.DrawRay(transform.position, dir,  Color.blue);

             if (hitInfo.collider.name == "Target"||hitInfo.collider.name == "wall"||hitInfo.collider.name == "collider"||hitInfo.collider.name == "lair_mouse")
             {
                 Debug.DrawRay(transform.position, dir,  Color.green);
                 _State.SetBool("AmIVisible", true);
 
             }else{
                 Debug.DrawRay(transform.position, dir,  Color.red);
                 _State.SetBool("AmIVisible", false);
 
             }
         }
         else
         {
             Debug.Log("Something else happened!");
         }
 
     }

    void OnDisable () {
        NavMesh.RemoveNavMeshData(m_NavMeshInstance);
    }

    public void Wander () {
         timer += Time.deltaTime;
        _agent.isStopped = false;
         if (timer >= wanderTimer) {
             Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
              _agent.SetDestination(newPos);
             timer = 0;
         }
    }

    public void Stop()
    {
        _agent.isStopped = true;
    }
    public void RunAway(){
        _agent.isStopped = false;

         Vector3 dirToPlayer = transform.position - hammer.transform.position;
         Vector3 newPos = transform.position + dirToPlayer;
         _agent.SetDestination(newPos);
    }

    
    private Transform startTransform;
    private float nextTurnTime;

    public void RunFrom()
    {
 
        // store the starting transform
        startTransform = transform;
         
        //temporarily point the object to look away from the player
        transform.rotation = Quaternion.LookRotation(transform.position - hammer.transform.position);
 
        //Then we'll get the position on that rotation that's multiplyBy down the path (you could set a Random.range
        // for this if you want variable results) and store it in a new Vector3 called runTo
        Vector3 runTo = transform.position + transform.forward * Random.Range(0f, 50.0f);
        //Debug.Log("runTo = " + runTo);
         
        //So now we've got a Vector3 to run to and we can transfer that to a location on the NavMesh with samplePosition.
         
        NavMeshHit hit;    // stores the output in a variable called hit
 
        // 5 is the distance to check, assumes you use default for the NavMesh Layer name
        NavMesh.SamplePosition(runTo, out hit, 5, 1 << NavMesh.GetNavMeshLayerFromName("Default")); 
        //Debug.Log("hit = " + hit + " hit.position = " + hit.position);
 
        // just used for testing - safe to ignore
        nextTurnTime = Time.time + 5;
 
        // reset the transform back to our start transform
        transform.position = startTransform.position;
        transform.rotation = startTransform.rotation;
 
        // And get it to head towards the found NavMesh position
        _agent.SetDestination(hit.position);
    }

    public void RunHide()
    {

        GameObject closesteForn = GetClosestForniture();
    }

    private GameObject GetClosestForniture()
    {
        GameObject closestFornit = null;
        float shorterDistance = 0;
        Forniture[] fornits = FindObjectsOfType<Forniture>();
        foreach (Forniture f in fornits)
        {
            float newDistance = Vector3.Distance(transform.position, f.transform.position);
            if (newDistance < shorterDistance || shorterDistance == 0f)
            {
                shorterDistance = newDistance;
                closestFornit = f.gameObject;
            }
        }

        return closestFornit;
    }

    public void StartLairCreation()
    {
        coroutine = CreateLair(10f);

        StartCoroutine(coroutine);
    }

    public void StopLairCreation()
    {
        StopCoroutine(coroutine);
    }
    
    public IEnumerator CreateLair(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        GameObject spawnerInstance = Instantiate(spawner, transform.parent);
        spawnerInstance.transform.position = transform.position;
        _State.Play("Moving");
        Wander();
        Debug.Log("wandering after instatiating");
    }


    // Update is called once per frame

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        Vector3 randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;
 
        NavMeshHit navHit;
 
        NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
 
        return navHit.position;
    }
}
