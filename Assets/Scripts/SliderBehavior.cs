﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderBehavior : MonoBehaviour
{
    private GameObject _nest;
    // Update is called once per frame
    void Update()
    {
        if (_nest != null)
        {
            var screenPos = Camera.main.WorldToScreenPoint(_nest.transform.position);
            screenPos += Vector3.up*2f;
            Cursor.visible = false;
            transform.position = Input.mousePosition;
        }
    }

    public void BindToNest(GameObject nest)
    {
        _nest = nest;
    }
}
