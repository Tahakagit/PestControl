﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Destroy : MonoBehaviour
{
    private GameDirector gM;
    private UIDirector IuI;
    private Canvas _canvas;
    public GameObject KillPoint;

    // Update is called once per frame
    void Update()
    {
        DestroyObject(transform.position, 2f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere(transform.position, 2f);
    }

    void DestroyObject(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
       
        foreach (Collider c in hitColliders)
        {

            
            if (c.gameObject.CompareTag("Mouse")) // can use this or layers to stop from hitting yourself
            {
                Debug.Log("Hit " + c.gameObject.name);


                float val = 50;

                GameDirector.instance.AddMoney(val);

                UIDirector.instance.DisplayScoreOnObject(c.gameObject, val, Color.green, 20);
                Debug.Log("About to destroy  " + c.gameObject.transform.parent.gameObject.name);
                Destroy(c.gameObject.transform.parent.gameObject);
            }

        }
    }

    private void StartUiScore(Vector3 screenPos, float value, Color color)
    {
        GameObject mouseValue = Instantiate(KillPoint, gameObject.transform.position, Quaternion.identity);
        mouseValue.transform.SetParent(_canvas.transform, false);
        mouseValue.transform.position = screenPos;
        Text textValue = mouseValue.GetComponent<Text>();

        textValue.text = value.ToString("C");

        textValue.color = color;
    }
    private void OnCollisionEnter(Collision other)
    {
       // Destroy((other.collider.gameObject));
    }
    
}

