﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.WSA;

public class LevelHud : MonoBehaviour
{

    public TextMeshProUGUI Score;
    public TextMeshProUGUI Timer;
    // Update is called once per frame
    private void Start()
    {
        throw new System.NotImplementedException();
    }

    void Update()
    {
        Score.text = GameDirector.instance.Score.ToString("c");
        Timer.text = LevelManager.instance.Timer.ToString();
    }

    public void Activate()
    {
        
        gameObject.SetActive(true);
    }
    public void Deactivate()
    {
        
        gameObject.SetActive(false);
    }

}
