﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillpointScript : MonoBehaviour
{
    bool waitActive = false; //so wait function wouldn't be called many times per frame
    public float rateOfFire;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * 10);

        if (waitActive == false)
        {

            StartCoroutine(Wait());
        }

    }
    IEnumerator Wait()
    {
        waitActive = true;
        yield return new WaitForSeconds(rateOfFire);
        Destroy(gameObject);

        waitActive = false;
    }

}
