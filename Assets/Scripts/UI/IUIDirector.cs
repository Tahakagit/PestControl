﻿using UnityEngine;

interface IUIDirector
{
    void DisplayScoreOnObject(GameObject target, float value, Color color, int fontSize);
}