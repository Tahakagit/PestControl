﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClickEvent : MonoBehaviour
{
    public void StartLevelSelection(int scene)
    {
        SceneManager.LoadScene(scene);
    }
}
