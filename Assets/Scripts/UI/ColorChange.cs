﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ColorChange : MonoBehaviour
{
    public UnityEvent UserLogged;
    // Start is called before the first frame update
    public void ChangeColor()
    {
        GetComponent<Image>().color = Color.red;
    }
    public void DefaultColor()
    {
        GetComponent<Image>().color = Color.clear;
    }

    private void Start()
    {
        UserLogged.AddListener(UIDirector.instance.StartMainMenu);
        UserLogged.AddListener(UIDirector.instance.StopSavegamePanel);

    }

    public void LoadGame()
    {
        
        //pass userinfo to game director
        GameDirector.instance.LoadGame(GetComponent<TextMeshProUGUI>().text,
            Convert.ToInt32(transform.Find("Score").GetComponent<TextMeshProUGUI>().text));

        UserLogged.Invoke();
    }

    public void DeleteSavegame()
    {
        SaveManager.DeleteUserByName(transform.parent.transform.Find("User").GetComponent<TextMeshProUGUI>().text);
    }

}
