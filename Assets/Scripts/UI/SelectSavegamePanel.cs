﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SelectSavegamePanel : MonoBehaviour
{
    public GameObject saveItem;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var save in GameDirector.instance.savesFound)
        {
            Debug.Log("Found " + save.PlayerName);
            TextMeshProUGUI userName;
            TextMeshProUGUI score;
            GameObject savePanel = Instantiate(saveItem, transform, false);
            userName = savePanel.transform.Find("User").GetComponent<TextMeshProUGUI>();
            score = userName.transform.Find("Score").GetComponent<TextMeshProUGUI>();

            //savePanel.transform.Find("Score");
            userName.text = save.PlayerName;
            score.text = save.HighScore.ToString();
            
            
                
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
