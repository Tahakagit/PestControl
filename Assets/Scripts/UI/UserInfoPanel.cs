﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UserInfoPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TextMeshProUGUI userName;
        TextMeshProUGUI score;
        userName = transform.Find("User").GetComponent<TextMeshProUGUI>();
        score = transform.Find("Score").GetComponent<TextMeshProUGUI>();

        userName.text = GameDirector.instance.loggedUser.ToString();
        score.text = GameDirector.instance.bestScore.ToString("c");
    }
}
