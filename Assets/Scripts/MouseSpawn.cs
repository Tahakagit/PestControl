﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MouseSpawn : MonoBehaviour
{
    bool waitActive = false; //so wait function wouldn't be called many times per frame

    public Slider lairHealth;
    private Slider lairHealthInstance;
    private IEnumerator coroutine;

    private float Health = 10;
    public float SpawnEvery;
    [FormerlySerializedAs("mouse")] public GameObject Mouse;
    // Start is called before the first frame update
    void Start()
    {
        coroutine = ShowSlider();
        lairHealthInstance = Instantiate(lairHealth);

        lairHealthInstance.gameObject.transform.SetParent(FindObjectOfType<Canvas>().transform);
    }

    private void FixedUpdate()
    {
        if (GameDirector.instance.GameStarted)
        {
            if (waitActive == false)
            {

                StartCoroutine(Wait());
                Spawn();
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Spawn()
    {
        Transform newPos = transform;
        GameObject mouseInstance = Instantiate(Mouse,transform.parent);
        mouseInstance.transform.position = transform.position;
    }
    
    IEnumerator Wait()
    {
        waitActive = true;
        yield return new WaitForSeconds(SpawnEvery);
        waitActive = false;
    }

    IEnumerator ShowSlider()
    {
        waitActive = true;
        lairHealthInstance.gameObject.SetActive(true);
        Vector3 newPos = transform.position;
        newPos += Vector3.up * 5; 
        lairHealthInstance.transform.position = Camera.main.WorldToScreenPoint(newPos);
        yield return new WaitForSeconds(5f);
        lairHealthInstance.gameObject.SetActive(false);

        waitActive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Cube")
        {
            GetComponent<Animator>().Play("lair_sbam");

            Debug.Log("collision!");
            Health -= 0.2f;
            lairHealthInstance.value = Health;
            StopCoroutine(coroutine);
            coroutine = ShowSlider();
            StartCoroutine(coroutine);

            //todo when health reach 0 kill lair and reset slider

            
            if (Health <= 0)
            {
                StopCoroutine(coroutine);
                
                Destroy(lairHealthInstance.gameObject);
                Destroy(gameObject);
            }
        }
    }

}
