﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Forniture : MonoBehaviour
{
    public float rateOfFire;
    
    private Rigidbody rig;
    private bool waitActive = false; //so wait function wouldn't be called many times per frame
    private bool _isStarted = false;
    private List<float> listOfVelocities;
    
    void Start()
    {
        listOfVelocities = new List<float>();
        rig = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //Debug.Log(GetTotalVelocity());
        if (!_isStarted && GetTotalVelocity() > 0)
        {
            _isStarted = true;
            listOfVelocities.Add(GetTotalVelocity());
        } else if (_isStarted && GetTotalVelocity() > 0)
            {
                listOfVelocities.Add(GetTotalVelocity());

        }else if (_isStarted&&GetTotalVelocity()<= 0)
        {
            _isStarted = false;
            double maxValue = Mathf.Max(listOfVelocities.ToArray());
            double newValue = System.Math.Round(maxValue, 1);
            //Debug.Log("Max Speed   " + newValue);
            if (maxValue>3f)
            {

               // Debug.Log(gameObject.name + "   " + maxValue);
                GameDirector.instance.SubtractMoney((float)maxValue);
                if (waitActive == false)
                {

                    StartCoroutine(ScoreAndWait(gameObject, (float)maxValue, Color.red));
                }

            }

            listOfVelocities.Clear();
        }
    }
    
    IEnumerator ScoreAndWait(GameObject screenPos, float value, Color color)
    {
        waitActive = true;
        UIDirector.instance.DisplayScoreOnObject(screenPos, value, color, 30);

        yield return new WaitForSeconds(rateOfFire);

        waitActive = false;
    }

    private float GetTotalVelocity()
    {
        float total = 0;
        Vector3 vel = rig.velocity;
        total += vel.x;
        total += vel.y;
        total += vel.z;
        
        
        
        return total;

    }
}
