﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Save
{
    public string PlayerName = null;
    public float HighScore = 0;

    public Save(string userName, float score)
    {
        PlayerName = userName;
        HighScore = score;
    }
}
