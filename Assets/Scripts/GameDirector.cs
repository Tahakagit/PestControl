﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class GameDirector : MonoBehaviour
{
    public float Score = 0;
    public bool GameStarted { get; set; }
    public string loggedUser;
    public int bestScore;

    private CameraDirector _cameraDirector;
    private ToolManager _toolManager;
    private UIDirector uI;
    private List<Save> saves;
    private Savegame savegame;
    private SaveManager saveM;
    public List<Save> savesFound;

    public static GameDirector instance = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);    

    }

    public void LoadGame(String user, int score)
    {
        loggedUser = user;
        bestScore = score;
    }
    
    void Start()
    {
            
        DontDestroyOnLoad(gameObject);
        saveM = FindObjectOfType<SaveManager>();
        saveM.LoadSaves();
        UIDirector.instance.StartSavegamePanel();

    }

    public void AddMoney(float amount)
    {
        Score += amount;
    }

    public void SubtractMoney(float amount)
    {
        Score -= amount;
    }
}
