﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolManager : MonoBehaviour
{
    public GameObject[] tools;
    static int currentTool = 0;
    private Vector3 _planePos;
    private Vector3 _mousePointInWorld;
    public GameObject tool{ get; private set; }
    private GameObject mountPoint;
    public bool IsDragging { get; set; }

    private GameDirector _gameDirector;
    private GameObject _cameraDirector;
    // Start is called before the first frame update
    void Start()
    {
        tool = tools[0];
        tool.SetActive(true);
        _cameraDirector = GameObject.Find("CameraDirector");

    }

    public void StartManager()
    {
        if (_cameraDirector.GetComponent<CameraDirector>().isToolActive)
        {
            IsDragging = true;
            Cursor.visible = false;

            mountPoint = GameObject.Find("MountPoint");
        }

    }
    public void StopManager()
    {
        if (!_cameraDirector.GetComponent<CameraDirector>().isToolActive)
        {
            IsDragging = false;
            //Cursor.visible = false;
            Destroy(tool);

        }

    }

    void Update()
    {
        //Cursor.visible = false;

        if (_cameraDirector.GetComponent<CameraDirector>().isToolActive)
        {
            if (IsDragging)
            {
                MoveObjectAlongMouse();
                SwitchTool();

            }
            //MoveObjectAlongMouse();
        }
    }

    public void MoveObjectAlongMouse()
    {

        _planePos = GameObject.Find("Floor").transform.position;
        _planePos.y -= .2f;

        Plane plane = new Plane(Vector3.up, _planePos);
        if (Camera.main != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float distance; // the distance from the ray origin to the ray intersection of the plane
            if (plane.Raycast(ray, out distance))
            {
                _mousePointInWorld = ray.GetPoint(distance);

                
                DragInsideArea();


             
            }
        }

    }

    private void DragInsideArea()
    {
        /**
         *
         * 10
         * 10/2 5
         * 5-10 -5
         */
        WallBehavior _raycaster = FindObjectOfType<WallBehavior>();

        if (IsBetween(_mousePointInWorld.x, _raycaster.minX, _raycaster.maxX))
        {
            if (IsBetween(_mousePointInWorld.z, _raycaster.minZ, _raycaster.maxZ))
            {
                mountPoint.transform.position = _mousePointInWorld;
            }
            else
            {
                Vector3 newPos = mountPoint.transform.position;
                newPos.x = _mousePointInWorld.x;
                mountPoint.transform.position = newPos;        

            }

        }
        else if (IsBetween(_mousePointInWorld.z, _raycaster.minZ, _raycaster.maxZ))
        {
            Vector3 newPos = mountPoint.transform.position;
            newPos.z = _mousePointInWorld.z;
            mountPoint.transform.position = newPos;        

        }

    }

    private void SwitchTool()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            tool = tools[0];
            tools[0].SetActive(true);
            tools[1].SetActive(false);

        }else if (Input.GetKey(KeyCode.Alpha2))
        {
            tool = tools[1];

            tools[1].SetActive(true);
            tools[0].SetActive(false);

        }
    }

    public void Stop()
    {
        IsDragging = false;

        tool.SetActive(false);
        Cursor.visible = true;

    }

    public void Resume()
    {
        IsDragging = true;

        tool.SetActive(true);
        Cursor.visible = false;

    }
    // Update is called once per frame
    
    private bool IsBetween(float value, float min, float max)
    {
        if (value <= max && value >= min)
        {
            return true;
        }
        else
        {
            return false;
        }
       
    }



}
