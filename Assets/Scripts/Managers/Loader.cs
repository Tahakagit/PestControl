﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{
    public GameObject gameDirector;
    public GameObject uiDirector;


    private void Start()
    {
        if (GameDirector.instance == null)
            //Instantiate gameManager prefab
            Instantiate(gameDirector);

        if (UIDirector.instance == null)
            //Instantiate gameManager prefab
            Instantiate(uiDirector);


    }
}
