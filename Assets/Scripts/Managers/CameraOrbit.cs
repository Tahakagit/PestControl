﻿using System;
using UnityEngine;

public class CameraOrbit : MonoBehaviour
{
    private Vector3 _planePos;
    protected Vector3 _pivot = new Vector3();
    protected CameraDirector _cameraDirector;
    public float RotateAmount = 15f;
    protected bool rightClick;
    ToolManager toolManager = null;

    // Start is called before the first frame update
    void Start()
    {
        _cameraDirector  = GetComponent<CameraDirector>();
        toolManager = FindObjectOfType<ToolManager>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LateUpdate()
    {
        if (_cameraDirector.isOrbiting)
        {
            OrbitCamera();
        }
    }

    public void OrbitCamera()
    {
        float x_rotate = Input.GetAxis("Camera Orbit") * RotateAmount;
        if (x_rotate != 0)
        {
            Vector3 mountPoint = FindObjectOfType<MountPoint>().transform.position;
            _cameraDirector.transform.RotateAround(mountPoint, Vector3.up, x_rotate);
            if (toolManager != null)
            {
                toolManager.IsDragging = false;
            }
        }
        else
        {
            if (toolManager != null)
                toolManager.IsDragging = true;
        }
    }
}