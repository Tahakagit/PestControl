﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDirector : MonoBehaviour
{
    public bool isOrbiting { get;  set; } = false;
    public bool isMoving { get; set; } = false;
    public bool isToolActive { get; set; } = false;
    public bool isHandActive { get; set; } = false;
    public bool isSelectingLevel { get; set; } = true;
    public static CameraDirector instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);    

    }

}

