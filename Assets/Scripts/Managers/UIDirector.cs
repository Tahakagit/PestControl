﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIDirector : MonoBehaviour,IUIDirector
{
    
    public GameObject ScoreOnObject;
    private GameObject selectUi;

    public GameObject levelHud;

    public GameObject gameOverPanel;

    public GameObject saveItem;
    public GameObject savegamesPanel;
    public GameObject createUserPanel;

    public GameObject mainScreenMenu;

    public GameObject userInfoPanel;
    public Canvas mainscreenCanvas;


    public InputField InputField;
    public static UIDirector instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

    private GameDirector gameManager;
    private LevelManager levelManager;

    private TextMeshProUGUI timeText;
    private TextMeshProUGUI scoreText;
    private Canvas _canvas;
    public string userName;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);    

    }
    
    void Start()
    {
        DontDestroyOnLoad(gameObject);

    }

    
    
    public void GameOver()
    {
        gameOverPanel = FindObjectOfType<GameOverPanel>().gameObject;
        gameOverPanel.SetActive(true);
    }
    
    public void DisplayScoreOnObject(GameObject target, float value, Color color, int fontSize)
    {
        GameObject mouseValue = Instantiate(ScoreOnObject, gameObject.transform.position, Quaternion.identity);
        var screenPos = Camera.main.WorldToScreenPoint(target.transform.position);

        _canvas = FindObjectOfType<Canvas>();
        //RectTransform rect = mouseValue.GetComponent<RectTransform>();
        mouseValue.transform.SetParent(_canvas.transform);

        mouseValue.transform.position = screenPos;

        TextMeshProUGUI textValue = mouseValue.GetComponent<TextMeshProUGUI>();
        textValue.fontSize = 18;
        mouseValue.transform.SetSiblingIndex(0);
        textValue.text = value.ToString("C");

        textValue.color = color;

    }


    public void StartSelectionMenu()
    {
        selectUi.SetActive(true);
    }

    public void StopSelectionMenu()
    {
        selectUi.SetActive(false);
    }

    public void StartMainMenu()
    {

        //mainScreenMenuInstance = Instantiate(mainScreenMenu, mainscreenCanvas.transform, false);
        mainScreenMenu.SetActive(true);

        mainScreenMenu.transform.Find("MenuPanel").transform.Find("Start").GetComponent<Button>().onClick.AddListener(
            delegate { SceneManager.LoadScene(1);});

    }
    public void StopMainMenu()
    {

        //mainScreenMenuInstance = Instantiate(mainScreenMenu, mainscreenCanvas.transform, false);
        mainScreenMenu.SetActive(false);

        mainScreenMenu.transform.Find("MenuPanel").transform.Find("Start").GetComponent<Button>().onClick.AddListener(
            delegate { SceneManager.LoadScene(1);});

    }


    public void StartSavegamePanel()
    {
        
        if (GameDirector.instance.savesFound.Count() > 0)
        {

            
            StopMainMenu();
            savegamesPanel.SetActive(true);
            Debug.Log("some savegame");

           
        }
        else
        {
            Debug.Log("No savegames found");
            //start user creation
            StartCreateUserPanel();
        }


    }

    public void StopSavegamePanel()
    {
        savegamesPanel.SetActive(false);
        //savegamesPanel.SetActive(false);
    }

    private void StartCreateUserPanel()
    {
        mainScreenMenu.SetActive(false);
        createUserPanel.SetActive(true);
        InputField textToSend = createUserPanel.GetComponentInChildren<InputField>();

        SaveManager saveM = FindObjectOfType<SaveManager>();
        Button btnSave = createUserPanel.GetComponentInChildren<Button>();
        btnSave.onClick.AddListener(delegate {saveM.AddFirstUser(textToSend.text); });
        
    }

    public void StopCreateUserPanel()
    {
        createUserPanel.SetActive(false);
    }
}