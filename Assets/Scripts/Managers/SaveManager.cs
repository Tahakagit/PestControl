﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager : MonoBehaviour
{

    public Savegame LoadSaves()
    {

//        Debug.Log("Deleting saved game");
//        File.Delete(Application.persistentDataPath + "/gamesave.save");

        List<Save> savesList = new List<Save>();

        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Savegame save = (Savegame)bf.Deserialize(file);

            // 3
            foreach (Save game in save.saves)
            {
                Debug.Log("Found " + game.PlayerName);
                savesList.Add(game);
            }
            file.Close();

            GameDirector.instance.savesFound = savesList;
            return save;
        }else{
            return null;
        }

    }

    public static void DeleteUserByName(string username)
    {
        List<Save> indexlist;

        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            BinaryFormatter bf2 = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Savegame save = (Savegame)bf.Deserialize(file);
            file.Close();
            FileStream fileToWrite = File.Create(Application.persistentDataPath + "/gamesave.save");

            // 3
            indexlist = save.saves;
            
            foreach (Save game in indexlist.ToList())
            {
                Debug.Log("Found " + game.PlayerName);
                //savesList.Add(game);
                if (game.PlayerName == username)
                {
                    Debug.Log(game.PlayerName + " Eliminato!");

                    //game.HighScore = 23;
                    save.saves.Remove(game);
                }
            }
            bf2.Serialize(fileToWrite, save);

            fileToWrite.Close();

        }else{
        }
    }
    
    
    public static void UpdateUserBestScore(string username)
    {
        List<Save> indexlist;

        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            BinaryFormatter bf2 = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Savegame save = (Savegame)bf.Deserialize(file);
            file.Close();
            FileStream fileToWrite = File.Create(Application.persistentDataPath + "/gamesave.save");

            indexlist = save.saves;
            
            foreach (Save game in indexlist.ToList())
            {
                Debug.Log("Found " + game.PlayerName);
                if (game.PlayerName == username)
                {
                    Debug.Log(game.PlayerName + " Nuovo bestscore!");

                    game.HighScore = GameDirector.instance.Score;
                }
            }
            bf2.Serialize(fileToWrite, save);

            fileToWrite.Close();

        }else{
        }
    }

    public void AddFirstUser(string username)
    {
        Savegame saveG = new Savegame();
        Save user = new Save(username, 0);
        saveG.AddSave(user);

        Debug.Log("Saved " + user.PlayerName);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, saveG);
        file.Close();

        UIDirector.instance.StopCreateUserPanel();
        

    }

/*
    public void AddUser(string username)
    {
        Savegame saveList = LoadSaves();
        if (saveList != null)
        {
            Save user = new Save(username);
            saveList.AddSave(user);
            
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenWrite(Application.persistentDataPath + "/gamesave.save");
            bf.Serialize(file, saveList);
            file.Close();

        }
        else
        {
        }
        

    }
*/
}
