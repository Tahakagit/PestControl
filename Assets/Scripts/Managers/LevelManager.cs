﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public int Timer;

    public List<GameObject> levels;
    public GameObject gameOverLost;
    public GameObject gameOverWin;
    
    public static LevelManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    private CameraDirector _cameraDirector;
    private ToolManager _toolManager;
    private UIDirector uI;


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);    

    }


    void Start()
    {


        StartLevelSelection();
    }



    private void StartLevelSelection()
    {
        Time.timeScale = 2;

        GameDirector.instance.GameStarted = false;
        _cameraDirector = FindObjectOfType<CameraDirector>();
        _cameraDirector.isMoving = true;

        //LoadLevels();
    }

    public void EnableControls()
    {
        _toolManager = FindObjectOfType<ToolManager>();

        _cameraDirector.isSelectingLevel = false;

        _cameraDirector.isMoving = true;
        _cameraDirector.isOrbiting = true;
        _cameraDirector.isHandActive = true;

        _cameraDirector.isToolActive = true;
        _toolManager.StartManager();

        GameDirector.instance.GameStarted = true;
    }

    private void StopControls()
    {
        _cameraDirector.isMoving = false;
        _cameraDirector.isOrbiting = false;
        _cameraDirector.isHandActive = false;

        _cameraDirector.isToolActive = false;

        _toolManager.StopManager();

    }
    public void GameOver()
    {
        StopControls();
        Cursor.visible = true;
        if (GameDirector.instance.Score > 0)
        {
            //success screen
            gameOverWin.SetActive(true);
            if (GameDirector.instance.Score > GameDirector.instance.bestScore)
            {
                SaveManager.UpdateUserBestScore(GameDirector.instance.loggedUser);
            }

        }else if(GameDirector.instance.Score <= 0)
        {
            
            //failure screen
            
            gameOverLost.SetActive(true);
        }
        Time.timeScale = 0;
    
    }

    public void StartTimer()
    {
        StartCoroutine(TimerCoroutine());

    }
    private IEnumerator TimerCoroutine()
    {
        for (int i = Timer; i > 0 ; i--)
        {
            yield return StartCoroutine(Wait(1));

            Timer--;
        }
        GameOver();


    }

    public IEnumerator Wait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime*Time.timeScale);
        //uI.DisplayTimer(timer);

    }

}
