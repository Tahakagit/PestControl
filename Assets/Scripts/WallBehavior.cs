﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBehavior : MonoBehaviour
{
    private Wall[] walls;
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;

    public Vector2 WalkableArea;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        walls = GameObject.FindObjectsOfType<Wall>();

        WallOcclusionCulling();
    }

    private void WallOcclusionCulling()
    {
        RaycastHit hitInfo;
        for (int i = 0; i < walls.Length; i++)
        {
            Vector3 dir = walls[i].transform.position - transform.position;

            dir += Vector3.up * walls[i].GetComponent<Collider>().bounds.extents.y;
            if (Physics.Raycast(transform.position, dir, out hitInfo, Mathf.Infinity))
            {
                //Debug.Log("detected" + hitInfo.collider.name);
                if (hitInfo.collider.gameObject.name == "collider")
                {
                    Debug.DrawRay(transform.position, dir, Color.red, 1);
                    hitInfo.collider.gameObject.transform.parent.GetComponent<MeshRenderer>().enabled = false;
                }
                else if (hitInfo.collider.gameObject.name == "wall")
                {
                    Debug.DrawRay(transform.position, dir, Color.green, 1);
                    hitInfo.collider.gameObject.GetComponent<MeshRenderer>().enabled = true;
                }else if (hitInfo.collider.gameObject.name == "floor")
                {
                    Debug.DrawRay(transform.position, dir, Color.blue, 1);

                    // square shaped based walkable area
                    minX = hitInfo.collider.gameObject.GetComponent<Collider>().bounds.min.x;
                    maxX = hitInfo.collider.gameObject.GetComponent<Collider>().bounds.max.x;
                    minZ = hitInfo.collider.gameObject.GetComponent<Collider>().bounds.min.z;
                    maxZ = hitInfo.collider.gameObject.GetComponent<Collider>().bounds.max.z;
//                    WalkableArea = new Vector2(x, z);
                }
            }
        }
    }

}
