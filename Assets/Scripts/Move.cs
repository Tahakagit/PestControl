﻿using UnityEngine;

public class Move : MonoBehaviour
{
    public float speed = 10;
    protected bool rightClick;
    protected CameraDirector _cameraDirector;


    private void Start()
    {
        _cameraDirector = GetComponent<CameraDirector>();
    }

    private void Update()
    {
        if (_cameraDirector.isMoving)
        {
            MoveByKeys();

        }
    }

    protected void MoveByKeys()
    {
        rightClick =Input.GetKey(KeyCode.Mouse1);
        if (rightClick)
        {
            //Camera.main.transform.LookAt(hammer.transform.Find("martello"));
            Camera.main.transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * Time.deltaTime * speed);

        }
        else
        {
            transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * Time.deltaTime * speed);

        }
        if (!rightClick)
        {
            transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * Time.deltaTime * speed);

        }

    }
}